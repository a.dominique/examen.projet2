#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 15:15:42 2017

@author: Alexis DOMINIQUE
"""
from setuptools import setup
setup(
    author="Alexis DOMINIQUE",
    author_email="alexis.dominique@bts-malraux.net",
    classifiers=[
        'Development Status :: 1 - Beta',
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: Français',
        'Operating System :: POSIC :: Linux',
        'Topic :: Multimedia :: Sound/Audio',
        'Topic :: Utilities',],
    description="Générateur de Playlist",
    entry_points={
        'console_scripts': [
            'programme-executable = programme:run'
            ]
        },
        install_requires=["lxml", "psycopg2"],
        keywords='radio playlist generator',
        license="GPLv3+",
        long_description="Générateur de playlist",
        name="PlaylistGenerator",
        packages=['PlaylistGenerator'],
        python_requires='>=1, <1.2',
        url="http://bts.bts-malraux.net/~a.dominique",
        version="1"
)
